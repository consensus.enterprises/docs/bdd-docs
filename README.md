# Behaviour Driven Development Docs

This is a Hugo docs "content pack" from Consensus Enterprises. You can pull it into your Hugo site as a git submodule:

`git submodule add https://gitlab.com/consensus.enterprises/docs/bdd-docs.git content/dev/bdd`
