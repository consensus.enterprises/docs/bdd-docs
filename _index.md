---
title: Testing
weight: 20
---

Behaviour-Driven Development
----------------------------

We use Behaviour-Driven Development ([BDD](https://en.wikipedia.org/wiki/Behavior-driven_development)) to ensure both:

* A clear, shared understanding of features and functionality by everyone from project sponsors to developers; and
* A test-first ([TDD](https://en.wikipedia.org/wiki/Test-driven_development)) approach to development.

Our tool of choice for BDD is [Behat](http://behat.org), which has extensions specifically for Drupal, MailHog, and so forth.

Testing Strategy
----------------

### Local Testing

__*The entire test suite ought to always pass locally.*__

Running tests locally should be as simple as:

```bash
make tests              # Run app-specific tests
make tests-wip          # Run tests for "Work-In-Progress" feature(s)
```

To only run tests with the `@wip` tag, run:

```
ddev exec bin/behat --tags=wip
```

### Continuous Integration

Dues to vagaries in the CI testing environment, we exclude certain classes of tests from our Continuous Integration suite. For example, it is notoriously difficult to set up JS-enabled browser testing in container-based (Docker) testing environments. So long as such tests are consistently tagged (e.e., `@javascript`) and pass [local testing](#local-testing), this should be acceptable.

Still, we should strive to run our entire test suite in CI.

